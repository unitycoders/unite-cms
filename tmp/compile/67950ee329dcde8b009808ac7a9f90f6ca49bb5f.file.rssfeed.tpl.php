<?php /* Smarty version Smarty3-b8, created on 2010-03-30 22:44:46
         compiled from "/opt/lampp/htdocs/uniteCMS/themes/uc_plain/rssfeed.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16055477044bb262becaf069-06043777%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '67950ee329dcde8b009808ac7a9f90f6ca49bb5f' => 
    array (
      0 => '/opt/lampp/htdocs/uniteCMS/themes/uc_plain/rssfeed.tpl',
      1 => 1269981856,
    ),
  ),
  'nocache_hash' => '16055477044bb262becaf069-06043777',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php echo '<?xml';?> version="1.0" encoding="UTF-8"?>
<rss version="2.0">
   <channel>
      <title><?php echo $_smarty_tpl->getVariable('title')->value;?>
 - Unity Coders</title>
      <link><?php echo $_smarty_tpl->getVariable('link')->value;?>
</link>
      <description><?php echo $_smarty_tpl->getVariable('description')->value;?>
</description>
      <language>en-gb</language>
      <pubDate>Tue, 10 Jun 2003 04:00:00 GMT</pubDate>
      <lastBuildDate>Tue, 10 Jun 2003 09:41:01 GMT</lastBuildDate>
      <docs>http://blogs.law.harvard.edu/tech/rss</docs>
      <generator>UniteCMS + Smarty</generator>
      <managingEditor>services@unitycoders.co.uk</managingEditor>
      <webMaster>webpigeon@unitycoders.co.uk</webMaster>


    <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['name'] = "items";
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['loop'] = is_array($_loop=($_smarty_tpl->getVariable('items')->value)) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["items"]['total']);
?>
      <item>
         <title><?php echo $_smarty_tpl->getVariable('title')->value;?>
</title>
         <link><?php echo $_smarty_tpl->getVariable('link')->value;?>
</link>
         <description><?php echo $_smarty_tpl->getVariable('description')->value;?>
</description>
         <pubDate>Tue, 03 Jun 2003 09:39:21 GMT</pubDate>
         <guid>http://liftoff.msfc.nasa.gov/2003/06/03.html#item573</guid>
      </item>
    <?php endfor; endif; ?>
   </channel>
</rss>
