<?php
/**
 *  Dispatch library - Routes urls based on regular expressions
 *  Copyright (C) 2010  Joseph Walton-Rivers
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Joseph Walton-Rivers <webpigeon@googlemail.com>
 *  @copyright Copyright (C) 2010 Joseph Walton-Rivers
 *  @license http://www.gnu.org/licenses/agpl.html GNU Affero Public License
 *  @package uniteCMS
 *  @subpackage libraries
 */

class DispatchLibrary{
    private $routes;
    private $cache;

    public function __construct($url, $routes = array()){
        $this->routes = $routes; 
        $this->matchRoute($url);
    }

    /**
     * Get a segment by name
     * @param name the name of the segment
     * @param default the default value for this segment
     * @return the value of the segment or default
     */
    public function getSegment($name, $default){
        if(!array_key_exists($name, $this->cache))
            return $default;
        return $this->cache[$name];
    }

    /**
     * Get all the segments from the url
     * @return an the array of segments
     */
    public function getSegments($default){
        $segments = array_intersect_key($this->cache, $default);
        return array_merge($segments, array_diff_key($default,$segments));
    }

    /**
     * XXX undocumented function
     */
    public function matchRoute($url){
        $matches = array();

        $routes = $this->fixRoutes($this->routes);
        foreach($routes as $route){   
            if(preg_match($route, $url, $matches))
                break;
        }

        if(empty($matches))
            throw new ErrorException("unknown route!");

        $this->cache = $matches;
    }

    /**
     * XXX undocumented function
     */
    private function fixRoutes($routes, $rules = array()){
        $rt = array(
            ':application:' => '(?<app>[a-z0-9_-]+)',
            ':controller:' => '(?<controller>[a-z0-9_-]+)',
            ':action:' => '(?<action>[a-z0-9_-]+)',
            ':ext:' => '(?<ext>[a-z0-9]{1,5})'
        );
        $rt = $rt + $rules;
        return str_replace(array_keys($rt), array_values($rt), $routes);
    }

    /**
     * adds a route to the route list
     * @param the route to be added
     */
    public function addRoute($route){
        $this->routes[] = $route;
    }
}
?>
