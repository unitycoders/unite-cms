<?php
/**
 *  <one line to give the program's name and a brief idea of what it does.>
 *  Copyright (C) <year>  <name of author>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Joseph Walton-Rivers <webpigeon@googlemail.com>
 *  @copyright Copyright (C) 2010 Joseph Walton-Rivers
 *  @license http://www.gnu.org/licenses/agpl.html GNU Affero Public License
 *  @package someProject
 *  @subpackage somethingElse
 */

 //register this as an autoloader
 spl_autoload_register('Factory::autoload');

 class Factory{
    private static $instance;
    private static $paths;

    public static function autoload($className){
        $i = self::getInstance();
        $p = $i->splitName($className);
        if(count($p) < 2)
            return;
        //    throw new ErrorException("$className couldn't be split");
        $i->loadClass($p[0], $p[1]);
        unset($p);
    }

    public static function setPaths($paths){
        self::$paths = $paths;
    }

    public static function &getInstance(){
        if(self::$instance == null){
            self::$instance = new Factory();
        }

        return self::$instance;
    }

    public function splitName($className){
        return preg_split('/(?=[A-Z][a-z0-9]+$)/', $className);
    }

    public function loadClass($class, $type){
        $path = self::path($type, $class);

        if(!$path)
            return;
	    //    throw new ErrorException("$class of type $type cannot be found.");
	    include($path);
   }

		static function path($type, $class)
		{
			$class	= basename(strtolower($class));
			$type	= basename(strtolower($type));

			if(self::$paths == null || !isset(self::$paths[$type]))
                return;
            //    throw new ErrorException("Unknown path for $type");

			return sprintf(self::$paths[$type], $class, $type);
		}
 }
?>
