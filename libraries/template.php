<?php
	class TemplateLibrary
	{
		private $themeView = null;

		public function __construct($theme)
		{
			$this->themeView = new viewLibrary("base.html", UC_PATH."/themes/$theme/");	
		}

		public function __set($name, $value)
		{
			$this->themeView->$name = $value;
		}

		public function __get($name)
		{
			return $this->themeView->$name;
		}

		public function __toString()
		{
			try{
				$this->themeView->display("base.html");
			}catch(Exception $e){
				echo $e;
			}
			return "";
		}
	}
?>
