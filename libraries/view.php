<?php
/**
 * UniteCMS view library - wrapper arround Smarty
 *
 * I designed UniteCMS this way so we can be indipendent of smarty
 * but still make use of the library, this way it should be easy
 * to swap out for something else if needed
 */

//XXX shouldn't the factory handle this?
require(UC_PATH."/vendors/smarty3/Smarty.class.php");

/**
 * View Library - renders templates
 */
class ViewLibrary{
  private $smarty;

  /**
   * Constructor for view library
   *
   * @deprecated constructor will change
   */
  public function __construct($path, $basePath){
     //setup smarty
     $this->smarty = new Smarty();
     $this->smarty->setCompileDir(UC_PATH."/tmp/compile");
     $this->smarty->setTemplateDir($basePath);
  }

  /**
   * Set a varable for the view
   */
  public function __set($name, $value){
    $this->smarty->assign($name, $value);
  }

  /**
   * Get a varable from the view
   */
  public function __get($name){
    throw new Exception("getting values not supported");
   }

  /**
   * Display a view
   */
   public function display($template){
     return $this->smarty->display($template);
    }
}
?>
