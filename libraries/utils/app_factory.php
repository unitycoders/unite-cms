<?php

//load the application file
require(UC_PATH."libraries/application.php");
require(UC_PATH."libraries/prototypes/controller.php");

/**
 * Builds an application
 */
function build_app($name, Website &$w){
	return new ApplicationLibrary($name, $w);
}

/**
 * Builds a controller
 */
function build_controller($name, ApplicationLibrary &$app){
	include($app->getPath().$name.".php");

	$name = ucwords($name)."Controller";
	return new $name($app);
}
