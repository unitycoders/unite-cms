<?php

function build_prototype($name){
	if(!class_exists($name."Prototype"))
		include("libraries/prototypes/$name.php");
}

function build_library($name, Website &$w){
	build_prototype("library");

	include("libraries/$name.php");
	$name = $name."Library";
	return new $name;
}
