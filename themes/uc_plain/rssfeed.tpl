<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
   <channel>
      <title>{$title} - Unity Coders</title>
      <link>{$link}</link>
      <description>{$description}</description>
      <language>en-gb</language>
      <pubDate>Tue, 10 Jun 2003 04:00:00 GMT</pubDate>
      <lastBuildDate>Tue, 10 Jun 2003 09:41:01 GMT</lastBuildDate>
      <docs>http://blogs.law.harvard.edu/tech/rss</docs>
      <generator>UniteCMS + Smarty</generator>
      <managingEditor>services@unitycoders.co.uk</managingEditor>
      <webMaster>webpigeon@unitycoders.co.uk</webMaster>


    {section name="items" loop="$items"}
      <item>
         <title>{$title}</title>
         <link>{$link}</link>
         <description>{$description}</description>
         <pubDate>Tue, 03 Jun 2003 09:39:21 GMT</pubDate>
         <guid>http://liftoff.msfc.nasa.gov/2003/06/03.html#item573</guid>
      </item>
    {/section}
   </channel>
</rss>
