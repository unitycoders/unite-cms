<?php
/**
 *  <one line to give the program's name and a brief idea of what it does.>
 *  Copyright (C) <year>  <name of author>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Joseph Walton-Rivers <webpigeon@googlemail.com>
 *  @copyright Copyright (C) 2010 Joseph Walton-Rivers
 *  @license http://www.gnu.org/licenses/agpl.html GNU Affero Public License
 *  @package someProject
 *  @subpackage somethingElse
 */
 //load core libraries
 require('libraries/core/events.php');
 require('libraries/core/factory.php');
 require('libraries/core/application.php');
 require('libraries/core/debug.php');

 class Website{
    private $config;
    private $application;

    public function __construct($configPath){
        $this->config = parse_ini_file($configPath, true);
        factory::setPaths($this->config['path']);
    }

    public function main(){
        if(!isset($_SERVER['PATH_INFO']))
            $pathData = "";
        else
            $pathData = $_SERVER['PATH_INFO'];

        try{
            $dispatch = new DispatchLibrary($pathData, $this->config['routes']);
            $arr = array('app'=>'system', 'controller'=>'homepage', 'action'=>'index', 'ext'=>'html');
            $arr = $dispatch->getSegments($arr);
            $this->callPage($arr['app'], $arr['controller'], $arr['action']);
        }catch(ErrorException $e){
            $this->callPage('system', 'errors', 'notfound'); 
        }
    }

    public function callPage($app, $controller, $action){
        include("applications/$app/$controller.php");
        $controller = ucwords($controller)."Controller";
        $app = new Application($app, new $controller($this));
        $app->callAction($action);
        die();

        //$path = UC_PATH."/applications/$app/$controller.php";
        //if(!file_exists($path))
        //    throw new ErrorException("Error Exception!");
        //include($path);
        //$controller = $controller."Controller";

        //if(!class_exists($controller))
        //    throw new ErrorException("Error Exception");
        //$controller = new $controller($this);

        //if(!method_exists($controller, $action))
        //    throw new ErrorException("$action");
        //$controller->$action();
        //die();
    }
 }
?>
